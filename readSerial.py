import serial
import time

# Replace '/dev/ttyUSB0' with your serial port and 9600 with your baud rate
ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)

# Open or create a file to store the data
with open('output.bin', 'wb') as file:
    start_time = time.time()
    while time.time() - start_time < 15:  # Capture for 15 seconds
        data = ser.read(ser.in_waiting or 1)
        if data:
            file.write(data)

ser.close()
