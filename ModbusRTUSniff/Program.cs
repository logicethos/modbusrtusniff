﻿using System.IO;

public static class Program
{
    static byte[] buffer = new byte[1000];
    private static int bufEnd =0;
    private static int sPointer =0;


    public static int Main(string[] args)
    {
#if DEBUG
        string filePath = "/home/stuart/modbus.data";  //test data

#else
        string filePath;
        if (args?.Length > 0 && Path.Exists(args[0]))
        {
            filePath = args[0];
        }
        else
        {
            Console.WriteLine("Usage: ModbusRTUSniff <modbus binary file>");
            return 1;
        }
#endif
        int nextSpointer = 0;
        using (FileStream fs = File.OpenRead(filePath))
        {
            int bytesRead;
            while ((bytesRead = fs.Read(buffer, bufEnd, buffer.Length - bufEnd)) > 0)
            {
                bufEnd += bytesRead;
                for (sPointer = nextSpointer; sPointer < bufEnd-5; sPointer++)  // potential frame start
                {
                    if (!IsStandardFunctionCode(buffer[sPointer+1])) continue;  //Unless byte 2 conforms to RTU frames, move on

                    //scan data, looking for matching CRC
                    for (int cPointer = sPointer+4; cPointer < bufEnd-2; cPointer++)
                    {
                        var crc = ModRTU_CRC(sPointer, cPointer);

                        if ((crc & 0xff) == buffer[cPointer+1] && crc >> 8 == buffer[cPointer + 2]) //CRC match
                        {
                            var skipped = sPointer - nextSpointer;
                            if (skipped > 0)
                            {
                                if (sPointer > 1)
                                {
                                    Console.WriteLine($"    NO CRC: Skipped {skipped} bytes {GetAddress(ref buffer, nextSpointer + 2):00000} {BitConverter.ToString(buffer, nextSpointer, 2)}..{BitConverter.ToString(buffer, sPointer - 2, 2)}  ");
                                }
                                else
                                {
                                    Console.WriteLine($"    NO CRC: Skipped {skipped} bytes {GetAddress(ref buffer, nextSpointer + 2):00000} {BitConverter.ToString(buffer, nextSpointer, 2)}  ");
                                }
                            }

                            //Capture RTU frame, and put in 'data'
                            var data = new byte[cPointer-sPointer+3];
                            Array.Copy(buffer, sPointer, data, 0, data.Length);

                            //Filter by packet type.  Very basic for Solis/SmartMeter use.
                            var bytecount = buffer[sPointer+2];
                            if (data.Length - 5 == bytecount)  //Data variable length.
                            {
                                Console.WriteLine($"{sPointer:000} Receive: {BitConverter.ToString(data, 0, 2)} Bytes: {bytecount} ({bytecount/2} registers)");
                            }
                            else if (data.Length == 8)  //Looks like a slave request frame
                            {
                                Console.WriteLine($"{sPointer:000} Request: {BitConverter.ToString(data, 0, 2)} Address: {GetAddress(ref data, 2):00000} -> {GetAddress(ref data, 4):00000}");
                            }
                            else //everything else
                            {
                                Console.WriteLine($"{sPointer:000}: {BitConverter.ToString(data, 0, 2)} {data.Length} unknown bytes:");
                            }
                            sPointer = cPointer + 2;
                            nextSpointer = sPointer + 1;
                            break;
                        }
                    }
                }

                if (nextSpointer < bufEnd)  //run out of data, shift unprocessed data to beginning of buffer
                {
                    bufEnd = bufEnd - nextSpointer  ;
                    Array.Copy(buffer, nextSpointer, buffer, 0, bufEnd);
                    nextSpointer = 0;
                }
            }
        }

        return 0;
    }

    static public UInt16 GetUInt16(ref byte[] data, int pointer)
    {
        return (UInt16)(data[pointer] << 8 | data[pointer+1]);

    }

    static public UInt32 GetUInt32(ref byte[] data, int pointer)
    {
        return (UInt32)(data[pointer] << 24 | data[pointer+1] << 16 | data[pointer+2] << 8 | data[pointer+3]);
    }


    //Calculate CRC between two buffer pointers
    static public ushort ModRTU_CRC(int start, int end)
    {
        UInt16 crc = 0xFFFF;

        for (int pos = start; pos <= end; pos++)
        {
            crc ^= (UInt16)buffer[pos]; // XOR byte into least sig. byte of crc

            for (int i = 8; i != 0; i--) // Loop over each bit
            {
                if ((crc & 0x0001) != 0) // If the LSB is set
                {
                    crc >>= 1; // Shift right and XOR 0xA001
                    crc ^= 0xA001;
                }
                else // Else LSB is not set
                    crc >>= 1; // Just shift right
            }
        }

        // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
        return crc;
    }

    static UInt16 GetAddress(ref byte[] data, int start)
    {
        return (UInt16)(data[start] << 8 | data[start+1]);
    }

    //Modbus RTU framing byte 2 is the message type.  Check against this list
    static bool IsStandardFunctionCode(byte code)
    {
        switch (code)
        {
           case 01: // Read Coils
           case 02: // Read Discrete Inputs
           case 03: // Read Holding Registers
           case 04: // Read Input Registers
           case 05: // Write Single Coil
           case 06: // Write Single Register
           case 07: // Read Exception Status
           case 08: // Diagnostics
           case 11: // Get Comm Event Counter
           case 12: // Get Comm Event Log
           case 15: // Write Multiple Coils
           case 16: // Write Multiple Registers
           case 17: // Report Slave ID
           case 20: // Read File Record
           case 21: // Write File Record
           case 22: // Mask Write Register
           case 23: // Read/Write Multiple Registers
           case 24: // Read FIFO Queue
               return true;
           default:
               return false;
        }
    }
}