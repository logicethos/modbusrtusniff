# ModbusRTUSniff

I needed to find out what data was being transmitted on an RS485 MODBUS RTU cable.
Without a logic analayser to hand, this is harder than it first appears, due the protocol using time delays to delimit the data (who came up with that idea!). 

The first thing I did, was capture some data:

```timeout 30s cat /dev/ttyAMA0 > modbus.data```

or you can try using the `readSerial.py` script.

Then I wrote this code to parse it.  It works using a pair of pointers in nested for-loops, looking for matching CRCs.
The second byte of every message, should fall in the range of the startdard RTU Function Codes, so it looks for that too to reduce wasted cpu cycles.

Once it's locked on, you dont need to mesure the time gaps, although for a more robust solution the data should be decoded fully.  I only covered what I needed (which was for a Smart Meter), so it may need more work for your use case.

### Usage

```ModbusRTUSniff <modbus binary file>```


### Executables

- [linux-arm64](https://gitlab.com/api/v4/projects/48140571/jobs/artifacts/main/raw/publish/linux-arm64/ModbusRTUSniff?job=linux-arm64)
- [linux-arm32](https://gitlab.com/api/v4/projects/48140571/jobs/artifacts/main/raw/publish/linux-arm32/ModbusRTUSniff?job=linux-arm32)
- [linux-x86](https://gitlab.com/api/v4/projects/48140571/jobs/artifacts/main/raw/publish/linux-arm32/ModbusRTUSniff?job=linux-x86)
- [win-x64](https://gitlab.com/api/v4/projects/48140571/jobs/artifacts/main/raw/publish/win-x64/ModbusRTUSniff?job=win-x64)
- [osx-x64](https://gitlab.com/api/v4/projects/48140571/jobs/artifacts/main/raw/publish/osx-x64/ModbusRTUSniff?job=osx-x64)


### Example Output
```
    NO CRC: Skipped 4 bytes 61951 00-4C..F1-FF  
004 Request: 01-04 Address: 00000 -> 00076
    NO CRC: Skipped 9 bytes 38979 01-04..43-6F  
021 Request: 01-04 Address: 00000 -> 00076
029 Receive: 01-04 Bytes: 152 (76 registers)
186 Request: 01-04 Address: 00342 -> 00002
194 Receive: 01-04 Bytes: 4 (2 registers)
203 Request: 01-04 Address: 00000 -> 00076
    NO CRC: Skipped 9 bytes 38979 01-04..43-6F  
220 Request: 01-04 Address: 00000 -> 00076
228 Receive: 01-04 Bytes: 152 (76 registers)
385 Request: 01-04 Address: 00000 -> 00076
393 Receive: 01-04 Bytes: 152 (76 registers)
550 Request: 01-04 Address: 00342 -> 00002
558 Receive: 01-04 Bytes: 4 (2 registers)
567 Request: 01-04 Address: 00342 -> 00002
575 Receive: 01-04 Bytes: 4 (2 registers)
584 Request: 01-04 Address: 00000 -> 00076
```